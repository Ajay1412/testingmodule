﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace WebAPIAjay.Models
{
	public class UserModel : MainModel
	{
		public int UserId { get; set; }
		[Required]
		public string UserName { get; set; }
		[Required]
		public string Password { get; set; }
		public bool Status { get; set; }

		public UserModel login(string userName, string password)
		{
			UserModel userModel = new UserModel();
			var userData = conn.Users.Where(x => x.Username == userName && x.Password == password).Select(x => x).FirstOrDefault();
			if (userData != null)
			{
				userModel.UserId = userData.UserId;
				userModel.UserName = userData.Username;
				return userModel;
			}
			else
				return userModel;
		}
	}
}