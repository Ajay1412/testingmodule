﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using WebAPIAjay.Configuration;

namespace WebAPIAjay.DataAccess
{
	public sealed class SqlDao
	{
		#region "Database Helper Methods"

		// Connection
		private SqlConnection _sharedConnection;
		public SqlConnection SharedConnection
		{
			get
			{
				if (_sharedConnection == null)
				{
					_sharedConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[DBConfiguration.DBProject].ConnectionString);
				}
				return _sharedConnection;
			}
			set { _sharedConnection = value; }
		}

		// Constructors
		public SqlDao() { }

		public SqlDao(SqlConnection connection)
		{
			this.SharedConnection = connection;
		}

		// GetDbSqlCommand
		public SqlCommand GetSqlCommand(string sqlQuery)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.Connection = SharedConnection;
			cmd.CommandType = CommandType.Text;
			cmd.CommandText = sqlQuery;
			return cmd;
		}

		// GetDbSprocCommand
		public SqlCommand GetDbSprocCommand(string sprocName)
		{
			SqlCommand cmd = new SqlCommand();
			cmd.Connection = SharedConnection;
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = sprocName;
			return cmd;
		}

		// CreateNullParameter
		public SqlParameter CreateNullParameter(string name, SqlDbType paramType)
		{
			SqlParameter parameter = new SqlParameter();
			parameter.SqlDbType = paramType;
			parameter.ParameterName = name;
			parameter.Value = DBNull.Value;
			parameter.Direction = ParameterDirection.Input;
			return parameter;
		}

		// CreateNullParameter - with size for nvarchars
		public SqlParameter CreateNullParameter(string name, SqlDbType paramType, int size)
		{
			SqlParameter parameter = new SqlParameter();
			parameter.SqlDbType = paramType;
			parameter.ParameterName = name;
			parameter.Size = size;
			parameter.Value = DBNull.Value;
			parameter.Direction = ParameterDirection.Input;
			return parameter;
		}

		// CreateOutputParameter
		public SqlParameter CreateOutputParameter(string name, SqlDbType paramType)
		{
			SqlParameter parameter = new SqlParameter();
			parameter.SqlDbType = paramType;
			parameter.ParameterName = name;
			parameter.Direction = ParameterDirection.Output;
			return parameter;
		}

		// CreateOuputParameter - with size for nvarchars
		public SqlParameter CreateOutputParameter(string name, SqlDbType paramType, int size)
		{
			SqlParameter parameter = new SqlParameter();
			parameter.SqlDbType = paramType;
			parameter.Size = size;
			parameter.ParameterName = name;
			parameter.Direction = ParameterDirection.Output;
			return parameter;
		}

		// CreateParameter - bool
		public SqlParameter CreateParameter(string name, bool value)
		{
			SqlParameter parameter = new SqlParameter();
			parameter.SqlDbType = SqlDbType.Bit;
			parameter.ParameterName = name;
			parameter.Value = value ? 1 : 0;
			parameter.Direction = ParameterDirection.Input;
			return parameter;
		}

		public SqlParameter CreateParameter(string name, DateTime value)
		{
			if (value == null)
			{
				// If value is null then create a null parameter
				return CreateNullParameter(name, SqlDbType.DateTime);
			}
			else
			{
				SqlParameter parameter = new SqlParameter();
				parameter.SqlDbType = SqlDbType.DateTime;
				parameter.ParameterName = name;
				parameter.Value = value;
				parameter.Direction = ParameterDirection.Input;
				return parameter;
			}
		}

		// CreateParameter - nvarchar
		public SqlParameter CreateParameter(string name, string value, int size)
		{
			if (String.IsNullOrEmpty(value))
			{
				// If value is null then create a null parameter
				return CreateNullParameter(name, SqlDbType.NVarChar);
			}
			else
			{
				SqlParameter parameter = new SqlParameter();
				parameter.SqlDbType = SqlDbType.NVarChar;
				parameter.Size = size;
				parameter.ParameterName = name;
				parameter.Value = value;
				parameter.Direction = ParameterDirection.Input;
				return parameter;
			}
		}

		public SqlParameter CreateParameter(string name, int value)
		{
			SqlParameter parameter = new SqlParameter();
			parameter.SqlDbType = SqlDbType.Int;
			parameter.ParameterName = name;
			parameter.Value = value;
			parameter.Direction = ParameterDirection.Input;
			return parameter;
		}

		public SqlParameter CreateParameter(string name, long value)
		{
			SqlParameter parameter = new SqlParameter();
			parameter.SqlDbType = SqlDbType.BigInt;
			parameter.ParameterName = name;
			parameter.Value = value;
			parameter.Direction = ParameterDirection.Input;
			return parameter;
		}

		public SqlParameter CreateParameter(string name, object value)
		{
			SqlParameter parameter = new SqlParameter();
			parameter.ParameterName = name;
			parameter.Value = value;
			parameter.Direction = ParameterDirection.Input;
			return parameter;
		}

		// CreateTextParameter
		public SqlParameter CreateTextParameter(string name, string value)
		{
			if (String.IsNullOrEmpty(value))
			{
				// If value is null then create a null parameter
				return CreateNullParameter(name, SqlDbType.Text);
			}
			else
			{
				SqlParameter parameter = new SqlParameter();
				parameter.SqlDbType = SqlDbType.Text;
				parameter.ParameterName = name;
				parameter.Value = value;
				parameter.Direction = ParameterDirection.Input;
				return parameter;
			}
		}
		#endregion

		#region "Data Projection Methods"
		public void ExecuteNonQuery(SqlCommand command)
		{
			try
			{
				if (command.Connection.State != ConnectionState.Open)
				{
					command.Connection.Open();
				}
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				throw new Exception("Error executing query", ex);
			}
			finally
			{
				command.Connection.Close();
			}
		}

		public Object ExecuteScalar(SqlCommand command)
		{
			try
			{
				if (command.Connection.State != ConnectionState.Open)
				{
					command.Connection.Open();
				}
				return command.ExecuteScalar();
			}
			catch (Exception ex)
			{
				throw new Exception("Error executing query", ex);
			}
			finally
			{
				command.Connection.Close();
			}
		}

		public T GetSingleValue<T>(SqlCommand command)
		{
			T returnValue = default(T);
			try
			{
				if (command.Connection.State != ConnectionState.Open)
				{
					command.Connection.Open();
				}
				SqlDataReader reader = command.ExecuteReader();
				if (reader.HasRows)
				{
					if (!reader.IsDBNull(0))
					{
						returnValue = (T)reader[0];
					}
					reader.Close();
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Error populating  query", ex);
			}
			finally
			{
				command.Connection.Close();
			}
			return returnValue;
		}

		// GetSingleInt32
		public Int32 GetSingleInt32(SqlCommand command)
		{
			Int32 returnValue = default(int);
			try
			{
				if (command.Connection.State != ConnectionState.Open)
				{
					command.Connection.Open();
				}
				SqlDataReader reader = command.ExecuteReader();
				if (reader.HasRows)
				{
					reader.Read();
					if (!reader.IsDBNull(0)) { returnValue = reader.GetInt32(0); }
					reader.Close();
				}
			}
			catch (Exception e)
			{
				throw new Exception("Error populating data", e);
			}
			finally
			{
				command.Connection.Close();
			}
			return returnValue;
		}

		// GetSingleString
		public string GetSingleString(SqlCommand command)
		{
			string returnValue = null;
			try
			{
				if (command.Connection.State != ConnectionState.Open)
				{
					command.Connection.Open();
				}
				SqlDataReader reader = command.ExecuteReader();
				if (reader.HasRows)
				{
					reader.Read();
					if (!reader.IsDBNull(0)) { returnValue = reader.GetString(0); }
					reader.Close();
				}
			}
			catch (Exception e)
			{
				throw new Exception("Error populating data", e);
			}
			finally
			{
				command.Connection.Close();
			}
			return returnValue;
		}

		// GetGuidList
		public List<Guid> GetGuidList(SqlCommand command)
		{
			List<Guid> returnList = new List<Guid>();
			try
			{
				if (command.Connection.State != ConnectionState.Open)
				{
					command.Connection.Open();
				}
				SqlDataReader reader = command.ExecuteReader();
				if (reader.HasRows)
				{
					returnList = new List<Guid>();
					while (reader.Read())
					{
						if (!reader.IsDBNull(0)) { returnList.Add(reader.GetGuid(0)); }
					}
					reader.Close();
				}
			}
			catch (Exception e)
			{
				throw new Exception("Error populating data", e);
			}
			finally
			{
				command.Connection.Close();
			}
			return returnList;
		}

		// GetStringList
		public List<string> GetStringList(SqlCommand command)
		{
			List<string> returnList = new List<string>();
			try
			{
				if (command.Connection.State != ConnectionState.Open)
				{
					command.Connection.Open();
				}
				SqlDataReader reader = command.ExecuteReader();
				if (reader.HasRows)
				{
					returnList = new List<string>();
					while (reader.Read())
					{
						if (!reader.IsDBNull(0)) { returnList.Add(reader.GetString(0)); }
					}
					reader.Close();
				}
			}
			catch (Exception e)
			{
				throw new Exception("Error populating data", e);
			}
			finally
			{
				command.Connection.Close();
			}
			return returnList;
		}

		// GetSingle
		public T GetSingle<T>(SqlCommand command) where T : class
		{
			T dto = null;
			try
			{
				if (command.Connection.State != ConnectionState.Open)
				{
					command.Connection.Open();
				}
				SqlDataReader reader = command.ExecuteReader();
				if (reader.HasRows)
				{
					reader.Read();
					IDataMapper mapper = new DataMapperFactory().GetMapper(typeof(T));
					dto = (T)mapper.GetData(reader);
					reader.Close();
				}
			}
			catch (Exception e)
			{
				throw new Exception("Error populating data", e);
			}
			finally
			{
				command.Connection.Close();
			}
			// return the DTO, it's either populated with data or null.
			return dto;
		}

		// GetList
		/// <summary>
		/// Gets the list.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="command">The command.</param>
		/// <returns></returns>
		public List<T> GetList<T>(SqlCommand command) where T : class
		{
			List<T> dtoList = new List<T>();
			try
			{
				if (command.Connection.State != ConnectionState.Open)
				{
					command.Connection.Open();
				}
				SqlDataReader reader = command.ExecuteReader();
				if (reader.HasRows)
				{
					IDataMapper mapper = new DataMapperFactory().GetMapper(typeof(T));
					while (reader.Read())
					{
						T dto = null;
						dto = (T)mapper.GetData(reader);
						dtoList.Add(dto);
					}
					reader.Close();
				}
			}
			catch (Exception e)
			{
				throw new Exception("Error populating data", e);
			}
			finally
			{
				command.Connection.Close();
			}
			// We return either the populated list if there was data,
			// or if there was no data we return an empty list.
			return dtoList;
		}
		#endregion
	}
}