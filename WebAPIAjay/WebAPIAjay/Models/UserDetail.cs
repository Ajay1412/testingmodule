﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPIAjay.Models
{
	public class UserDetail
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public DateTime DOB { get; set; }
		public string Gender { get; set; }
	}
}