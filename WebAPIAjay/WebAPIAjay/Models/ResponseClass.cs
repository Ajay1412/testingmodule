﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPIAjay.Models
{
	public class ResponseClass
	{
		public MessageType MessageType { get; set; }
		public string Message { get; set; }
		public List<dynamic> Data { get; set; }
	}

	public enum MessageType
	{
		Success,
		Error,
		Warn
	}
}