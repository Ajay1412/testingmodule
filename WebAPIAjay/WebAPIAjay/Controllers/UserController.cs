﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Cors;

using WebAPIAjay.Models;

namespace WebAPIAjay.Controllers
{
	public class UserController : MainController
	{
		public UserModel userModel;

		public UserController() {
			userModel = new UserModel();
		}

		[Authorize]
		[HttpPost]
		[Route("api/user/login")]
		public ResponseClass Login(UserModel user)
		{
			responseObject.Message = "ddd";
			UserModel userData = userModel.login(user.UserName, user.Password);
			if (userData.UserId > 0) {
				responseObject.Message = "Login Successfully.";
				responseObject.MessageType = MessageType.Success;
			}
			return responseObject;
		}
	}
}