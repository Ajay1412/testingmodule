﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using System.Threading.Tasks;
using WebAPIAjay.Service;

namespace WebAPIAjay.App_Start
{
	internal class AuthorizationServerProvider : OAuthAuthorizationServerProvider
	{
		private UserService userService;

		public AuthorizationServerProvider()
		{
			this.userService = new UserService();
		}

		public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
		{
			//string clientId;
			//string clientSecret;
			//context.TryGetFormCredentials(out clientId, out clientSecret);

			//if (clientId == "1234" && clientSecret == "12345")
			//{
			//	context.Validated(clientId);
			//}

			// Should really do some validation here :)

			
			//var user = userService.GetUser(context.UserName, context.Password);
			context.Validated();
			
			//return base.ValidateClientAuthentication(context);
		}

		public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
		{
			var user = userService.GetUser(context.UserName, context.Password);
			if (user.UserId > 0)
			{
				var oAuthIdentity = new ClaimsIdentity(context.Options.AuthenticationType);
				oAuthIdentity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
				var ticket = new AuthenticationTicket(oAuthIdentity, new AuthenticationProperties());
				context.Validated(ticket);
			}
			else
			{
				context.SetError("invalid_grant", "Provided username and password is incorrect");
				context.Rejected();
			}
			return;
		}

		public void GrantRefreshToken()
		{
		}
	}
}