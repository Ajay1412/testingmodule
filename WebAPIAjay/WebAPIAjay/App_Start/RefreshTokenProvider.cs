﻿using System.Threading.Tasks;
using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Collections.Concurrent;
using Microsoft.Owin.Security;

namespace WebAPIAjay.App_Start
{
	internal class RefreshTokenProvider : IAuthenticationTokenProvider
	{
		private static ConcurrentDictionary<string, AuthenticationTicket> _refreshTokens = new ConcurrentDictionary<string, AuthenticationTicket>();

		public void Create(AuthenticationTokenCreateContext context)
		{
			throw new System.NotImplementedException();
		}

		public async Task CreateAsync(AuthenticationTokenCreateContext context)
		{
			var guid = Guid.NewGuid().ToString();


			_refreshTokens.TryAdd(guid, context.Ticket);

			// hash??
			context.SetToken(guid);
		}

		public void Receive(AuthenticationTokenReceiveContext context)
		{
			throw new System.NotImplementedException();
		}

		public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
		{
			AuthenticationTicket ticket;

			if (_refreshTokens.TryRemove(context.Token, out ticket))
			{
				context.SetTicket(ticket);
			}
		}
	}
}